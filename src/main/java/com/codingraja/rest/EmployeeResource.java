package com.codingraja.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codingraja.domain.Employee;

@RestController
@RequestMapping("/api")
public class EmployeeResource {
	
	@GetMapping("/employees")
	public Employee getEmployee() {
		return new Employee(1001L, "CL", "Verma", "info@codingraja.com", 50000.0);
	}

}
